# Using vcpkg for dependency management

[vcpkg](https://vcpkg.io/) can be used for external/thirdparty
 dependencies that aren't part of the repository.

## Install vcpkg

```
$ git clone --recursive https://github.com/Microsoft/vcpkg.git
```

```
$ cd vcpkg && ./bootstrap-vcpkg.sh/bat
```

## Configure the CMake build to use the vcpkg CMake toolchain file

There are probably other ways to do this but here is an example configuration
for a user level configuration for CMakeTools extension of VS Code.

### Using CMakeTools kits

On Windows this file is located at ```$User/AppData/Local/CMakeTools/cmake-tools-kits.json```

```
[
  {
    "name": "Visual Studio Professional 2022 Release - amd64",
    "visualStudio": "ad37be74",
    "visualStudioArchitecture": "x64",
    "toolchainFile": "D:/Data/devel/vcpkg/scripts/buildsystems/vcpkg.cmake",
    "preferredGenerator": {
      "name": "Visual Studio 17 2022",
      "platform": "x64",
      "toolset": "host=x64"
    }
  }
```

### Using CMake Presets with VS Code

See the [vscode-cmake-tools documentation](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/cmake-presets.md) and the [CMake Preset documenation](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html)

The included CMake presets require Ninja to be used as the CMake Generator. Add
Ninja to PATH so it can be found by CMake. I've used the Ninja included with
Visual Studio.

Optional: Add clang-format and clang-tidy to PATH.


## libsndfile

```
$ ./vcpkg.exe install --triplet x64-windows libsndfile
```

vcpkg still uses x86-windows as the default triplet, although the default is
changing on Windows to x64-windows.

## curl

```
$ ./vcpkg.exe install --triplet x64-windows curl
```

## portaudio

```
$ ./vcpkg.exe install --triplet x64-windows curl
```

## GTest

```
$ ./vcpkg.exe install gtest:x64-windows
```

# Configure launch.json

See the [documentation](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/debug-launch.md#debugging-tests)

Example

```
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "(msvc) Launch",
            "type": "cppvsdbg",
            "request": "launch",
            // Resolved by CMake Tools:
            "program": "${command:cmake.launchTargetPath}",
            "args": [],
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}",
            "environment": [
                {
                    // add the directory where our target was built to the PATHs
                    // it gets resolved by CMake Tools:
                    "name": "PATH",
                    "value": "${env:PATH}:${command:cmake.getLaunchTargetDirectory}"
                },
                {
                    "name": "OTHER_VALUE",
                    "value": "Something something"
                }
            ],
            "console": "externalTerminal"
        },
        {
            "name": "CTestDebug",
            "type": "cppdbg",
            "request": "launch",
            "stopAtEntry": true,
            "program": "${cmake.testProgram}",
            "args": [ "${cmake.testArgs}" ],
            "windows": {
                "type": "cppvsdbg"
            }
        }
    ]
}
```