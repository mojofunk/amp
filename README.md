# A Meta Project (AMP)

This repository includes standalone software projects as git submodules
to build as a single "project".

The projects are currently CMake based and one of the purposes of this
repository is to test and ensure that the projects don't have any issues
when being used as subprojects.

It also acts as a "blessed" or tested set of versions of library
dependencies on platforms like Windows where dependency management is
difficult(currently).

# Libraries

The [fmt](https://github.com/fmtlib/fmt) library, currently using version 7.1.3.

The [adt](https://gitlab.com/mojofunk/adt) Logging and Instrumentation
library.

The [qadt](https://gitlab.com/mojofunk/qadt) library, a Qt based GUI for
[adt](https://gitlab.com/mojofunk/adt)

# Library Dependencies

Where those dependencies are not included as submodules and
are required to be found somehow via CMake this section provides some guidance
on how to install and configure those.

Using [vcpkg](docs/vcpkg.md)

## External libraries

[GTest](https://github.com/google/googletest) doesn't export targets yet
(1.8.0). Could be privately included in libraries that require it (this
is recommended).

[Qt](https://qt.io) version 5. Does not support building with CMake.


## TODO

Add and use [Google Benchmark](https://github.com/google/benchmark)

Remove portaudio and use JUCE for audio I/O.

# Suggested Development Dependencies

## clang-format

## clang-tidy

# Packaging Tools

[Conan](https://conan.io) might make it easier to manage dependencies
like Qt, that don't have native CMake build support.

# Cloning

As this repository contains submodules you will need to clone with:

`git clone --recursive ...`

or preferably with a more recent git:

`git clone --recursive -j8 ...`

# Adding Submodules

As of git version 1.8.2 you can specify which branch to track with:

`git submodule add -b master [URL to Git repo]`

# Update Submodules

`git submodule update --remote`
