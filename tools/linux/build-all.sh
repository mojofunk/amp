#! /bin/bash

for CONFIG_PATH in configurations/*
do
	CONFIG_NAME=`basename $CONFIG_PATH`
	. src/env
	. src/build
done
