#! /bin/bash

. src/env

for DIR in build/*/
do
	if [ -f $DIR/CTestTestfile.cmake ]; then
		cd $DIR && ctest
	else
		echo "Configuration in $DIR doesn't have tests enabled"
	fi
	cd $SCRIPT_DIR
done
