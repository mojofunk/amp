#! /bin/bash

if [ $1 ]; then
	if [ ! -f "configurations/$1" ]; then
		echo "Configuration $1 does not exist."
		echo "Possible Configurations:"
		for CONFIG_PATH in configurations/*
		do
			CONFIG_NAME=`basename $CONFIG_PATH`
			echo "$CONFIG_NAME"
		done
		exit
	fi
fi

. src/commandline
. src/env
. src/configure
