#! /bin/bash

export DESTDIR=./tmp

for CONFIG_PATH in configurations/*
do
	CONFIG_NAME=`basename $CONFIG_PATH`
	. src/env
	. src/install
done
