#! /bin/bash

# override config
CONFIG_NAME=debug-static-coverage

. src/env

if [ ! -d $BUILD_DIR ]; then
	. src/configure
fi

. src/build && cd $BUILD_DIR || exit

ctest && gcovr
