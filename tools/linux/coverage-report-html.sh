#! /bin/bash

# override config
CONFIG_NAME=debug-static-coverage

. src/env

if [ ! -d $BUILD_DIR ]; then
	. src/configure
fi

. src/build && cd $BUILD_DIR || exit

COVERAGE_HTML_DIR=coverage-html

# This will run all tests and generate coverage data
ctest || exit

mkdir -p $COVERAGE_HTML_DIR || exit

gcovr . --html --html-details -o $COVERAGE_HTML_DIR/amp.html

gnome-open $COVERAGE_HTML_DIR/amp.html
